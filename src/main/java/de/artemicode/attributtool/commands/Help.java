package de.artemicode.attributtool.commands;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class Help implements ICommand {

	@Override
	public String getCommandName() {
		return "help";
	}

	@Override
	public String getDescription() {
		return "Show this help";
	}

	@Override
	public String getUsage() {
		return null;
	}

	@Override
	public void execute(String[] args) {
		System.out.println("Attribut Tool - this tool is CC0 licensed");
		System.out.println("Use command \"license\" to show details and 3rd party components");
		System.out.println();
		System.out.println("Usage: attribut_tool.jar [commandName] {parameters}");
		System.out.println("Available commands: [parameter] is mandatory, {parameter} is optional");
		System.out.println();

		ServiceLoader<ICommand> commandServiceLoader = ServiceLoader.load(ICommand.class);
		Iterator<ICommand> commandsIterator = commandServiceLoader.iterator();
		List<ICommand> commands = new ArrayList<>();
		commandsIterator.forEachRemaining(commands::add);
		commands.sort(Comparator.comparing(ICommand::getCommandName));

		int commandLen = commands.stream().mapToInt(
				(c) -> c.getCommandName().length()).max()
				.orElseThrow(IllegalStateException::new);

		int columnSize = commandLen + 2;

		for (ICommand command : commands) {
			outputColumn(columnSize, command.getCommandName(), command.getDescription());
			if (command.getUsage() != null) {
				outputColumn(columnSize, null, command.getUsage());
			}
		}
	}


	private void outputColumn(int columnSize, String firstColumn, String secondColumn) {
		int fill = firstColumn == null ? columnSize : columnSize - firstColumn.length();
		StringBuilder builder = new StringBuilder();
		if (firstColumn != null) {
			builder.append(firstColumn);
		}
		for (int i=0;i<fill;++i) {
			builder.append(' ');
		}
		if (secondColumn != null) {
			builder.append(secondColumn);
		}
		System.out.println(builder.toString());
	}
}
