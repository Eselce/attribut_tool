package de.artemicode.attributtool.tools;

import java.util.Iterator;

import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;

public class SetRectifier {

	public void rectifySet(AttributSet attributSet) {

		int setLines = 0;

		for (AttributCard card : attributSet.getCards()) {
			for (int i=0;i<card.getLines().size();++i) {
				String line = card.getLines().get(i);
				if (line.trim().isEmpty()) {
					card.getLines().set(i, " ");
				} else {
					card.getLines().set(i, line.trim());
					if (setLines < (i+1)) {
						setLines = i+1;
					}
				}
			}
		}

		Iterator<AttributCard> cardIterator = attributSet.getCards().iterator();
		while (cardIterator.hasNext()) {
			AttributCard card = cardIterator.next();
			boolean emptyCard = true;
			for (String line : card.getLines()) {
				if (!line.trim().isEmpty()) {
					emptyCard = false;
					break;
				}
			}
			if (emptyCard) {
				cardIterator.remove();
				continue;
			}
			while (card.getLines().size() < setLines) {
				card.getLines().add(" ");
			}
		}

		attributSet.setAttributeCount(attributSet.getCards().size());
	}

}
