package de.artemicode.attributtool.commands;

import de.artemicode.attributtool.model.AttributCard;
import de.artemicode.attributtool.model.AttributSet;
import de.artemicode.attributtool.tools.CardMeasurer;
import de.artemicode.attributtool.tools.ReadSetTool;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class DisplayOverflow implements ICommand {

	@Override
	public String getCommandName() {
		return "displayOverflow";
	}

	@Override
	public String getDescription() {
		return "Display the overflow of the cards (text too large)";
	}

	@Override
	public String getUsage() {
		return "[attributSetFile]";
	}

	@Override
	public void execute(String[] args) {
		if (args.length < 2) {
			System.err.println("No filename given.");
			return;
		}
		AttributSet set = ReadSetTool.readSet(args[1]);
		if (set == null) {
			return;
		}

		CardMeasurer measurer = new CardMeasurer();
		for (AttributCard card : set.getCards()) {
			StringBuilder overflowInfo = new StringBuilder();
			StringBuilder cardText = new StringBuilder();
			boolean first = true;
			for (String line : card.getLines()) {
				int textWidth = measurer.measure(line);
				int overflow = textWidth - 88;
				if (!first) {
					overflowInfo.append('|');
					cardText.append('|');
				} else {
					first = false;
				}
				overflowInfo.append(overflow <= 0 ? "OK" : overflow);
				cardText.append(line);
			}

			System.out.println(overflowInfo.toString() + " " + cardText.toString());

		}
	}
}
