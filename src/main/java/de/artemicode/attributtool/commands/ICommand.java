package de.artemicode.attributtool.commands;

public interface ICommand {

	String getCommandName();

	String getDescription();

	String getUsage();

	void execute(String[] args);

}
